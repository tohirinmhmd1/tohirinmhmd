
public class Programmer extends Person{
	String technology;
	
	public Programmer() {
		super();
	}
	
	public Programmer(String name, String address, String technology) {
		super(name, address);
		this.technology = technology;
	}
	void hacking() {
		System.out.println("I Can hacking a website");
	}
	
	void coding() {
		System.out.println("I Can create a application using technology : " + technology + ".");
	}
	
	void greeting() {
		super.greeting();
		System.out.println("My job is a " + technology + " Programmer.");
	}
}
