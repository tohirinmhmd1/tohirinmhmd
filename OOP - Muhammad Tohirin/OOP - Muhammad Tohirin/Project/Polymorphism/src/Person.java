
public class Person {
	String name;
	String address;
	
	public Person(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}

	public Person() {
		super();
	}
	
	void greeting() {
		System.out.println("Hello Myname is " + name + ".");
		System.out.println("I, Come from " + address + ".");
	}
}
